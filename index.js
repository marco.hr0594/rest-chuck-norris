var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var path = require('path');

var app = express();
var port = process.env.PORT || 3525;

// Convierte una petición recibida (POST-GET...) a objeto JSON
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

var admin = require(path.join(__dirname, '/routes/jokes'));
app.use('/chucknorris', admin);

app.get('/', function (req, res) {
	res.redirect("/chucknorris/joke");
})

app.listen(port, function(){  
	console.log(`Server running in http://localhost:${port}`);
	console.log('Defined routes:');
	console.log('	[GET] http://localhost:3525/');
});