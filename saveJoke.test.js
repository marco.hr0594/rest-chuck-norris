var MongoClient = require('mongodb');

describe('insert', () => {
  let connection;
  let db;
  var url = 'mongodb://nb8:nb8@myfirstprojectlanding-shard-00-00.1hh6n.mongodb.net:27017,myfirstprojectlanding-shard-00-01.1hh6n.mongodb.net:27017,myfirstprojectlanding-shard-00-02.1hh6n.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=MyFirstProjectLanding-shard-0&authSource=admin&retryWrites=true&w=majority&ssl=true';


  beforeAll(async () => {
    connection = await MongoClient.connect(url, {
      useNewUrlParser: true,
    });
    db = await connection.db('Jokes');
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  it('should insert a doc into collection', async () => {
    const users = db.collection('Jokes');

    const mockUser = {joke: 'some-user-id', url_icon: 'John'};
    await users.insertOne(mockUser);

    const insertedUser = await users.findOne({joke: 'some-user-id'});
    expect(insertedUser).toEqual(mockUser);
  });
});