
var mongoose = require('mongoose');
var url = 'mongodb://nb8:nb8@myfirstprojectlanding-shard-00-00.1hh6n.mongodb.net:27017,myfirstprojectlanding-shard-00-01.1hh6n.mongodb.net:27017,myfirstprojectlanding-shard-00-02.1hh6n.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=MyFirstProjectLanding-shard-0&authSource=admin&retryWrites=true&w=majority&ssl=true';

try {
    mongoose.connect( url, { useUnifiedTopology: true, useNewUrlParser: true }, () =>
    console.log("connected"));    
}catch (error) { 
    console.log("could not connect");    
}

module.exports = mongoose; 