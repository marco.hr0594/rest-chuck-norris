var express = require('express');
var router = express.Router();
var request = require('request');
var Jokes = require('../models').Jokes; 


// URL con contenido JSON demostrativo.
var url 	= "https://api.chucknorris.io/";

router.get('/joke', function(req, res){
	request({
	    url: url + 'jokes/random',
	    json: false
	}, function (error, response, body) {

	    if (!error && response.statusCode === 200) {
	        res.send(JSON.parse(body).value);
	    }
	})
});
router.get('/joke/categories', function(req, res){
	request({
	    url: url + 'jokes/categories',
	    json: false
	}, function (error, response, body) {

	    if (!error && response.statusCode === 200) {
	        res.send(body);
	    }
	})
});


router.get('/joke/random',  function(req, res) {
    console.log("Category " + req.query.category);
    if(req.query.category == '' || !req.query.category ){
        res.send("Debe mandar un valor en la categoria");
    } 

    var queryObject = {
        category: req.query.category,
    };
	request({
	    url: url + 'jokes/random',
        qs: queryObject,
	    json: false
	}, function (error, response, body) {

	    if (!error && response.statusCode === 200) {
            res.send(JSON.parse(body).value); 
	    } else {
            res.send(JSON.parse(response.body).message);
        }
	})
});

router.get('/joke/search',   function(req, res) {
    console.log("Search " + req.query.query);
    if(req.query.query == '' || !req.query.query ){
        res.send("Debe mandar un valor en la categoria");
    } 

    var queryObject = {
        query: req.query.query,
        email : req.query.email || 'extreme_hardy@live.com.mx',
    };
	request({
	    url: url + 'jokes/search',
        qs: queryObject,
	    json: false
	},  async function (error, response, body) {

	    if (!error && response.statusCode === 200) {
            var sendEmail =  sendResultsJokes(body);
            console.log("Se mando el email " + sendEmail);
            res.send(body); 
	    } else {
            res.send(JSON.parse(response.body).message);
        }
	})
});

function  sendResultsJokes (body , email ) {
    

    var  sgMail = require('@sendgrid/mail')
    sgMail.setApiKey('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')
    var body = "<!DOCTYPE html>" + "<html lang='es'>" + "<head>" + "<meta charset='utf-8'>" + "<meta http-equiv='X-UA-Compatible' content='IE=edge'>" + "<meta name='viewport' content='width=device-width, initial-scale=1'>" + "</head>" + "<!-- Body TAG-->" + "<body>" + "<div class='col-md-6 col-xs-6' align='center' style='background-color:#777171;'><img src='https://api.chucknorris.io/img/chucknorris_logo_coloured_small@2x.png' class='logo' width='150px' height='80px'></div>" + "<div class='container'>" + "<h4>Los resultados de la búsqueda: </h4>" + "<p>Nombre: " + body.result + "</p>"+ "</div> " + "</body><!-- ./End Body TAG-->" + "</html>";

    var msg = {
    to: 'extreme_hardy@live.com.mx', // Change to your recipient
    from: 'extreme_hardy@live.com.mx', // Change to your verified sender
    subject: 'Sending with SendGrid is Fun',
    text: 'and easy to do anywhere, even with Node.js',
    html: body
    }

    sgMail
    .send(msg)
    .then((response) => {
        console.log(response[0].statusCode)
        //console.log(response[0].headers)
    })
    .catch((error) => {
        console.error(error)
    })
}
 
router.get('/' , function(req, res) {
	const user = new Jokes({ name: 'eldevsin.site' , icon_url : 'sdsd' }); // crea la entidad
	user.save(); // guarda en bd
});


module.exports = router; 